package com.gameoflife;

import java.util.Random;

public class Grid {


    final int[][] GRID;

    int[][] tempGrid = new int[50][50];

    Grid() {
        GRID = new int[50][50];
        randomGen();
    }

    public void iteration() {
        for (int iter = 0; iter < 100; iter++) {
            draw();
            numOfNeighbors();
            nextGeneration();
            modifyGrid();
            drawNext();
        }
    }

    private void randomGen() {    // to fill array
        Random random = new Random();
        for (int row = 0; row < GRID.length; row++) {
            for (int col = 0; col < GRID[row].length; col++) {
                GRID[row][col] = random.nextInt(2);
            }
        }
    }

    public void draw() {          // to print array on the screen
        for (int i = 0; i < GRID.length; i++) {
            for (int j = 0; j < GRID[i].length; j++) {
                if (GRID[i][j] == 0) {      //0 = " ", 1 = "Х"
                    System.out.print(" ");
                } else {
                    System.out.print("X");
                }
            }
            System.out.println();
        }
    }

    public int numOfNeighbors() {        // to count of neighbors

        int countOfNeighbors = 0;

        for (int i = 0; i < GRID.length; i++) {
            for (int j = 0; j < GRID[i].length; j++) {

                if (GRID[transform(i++)][transform(j--)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i++)][transform(j)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i++)][transform(j++)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i)][transform(j++)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i--)][transform(j++)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i--)][transform(j)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i--)][transform(j--)] == 1) {
                    countOfNeighbors++;
                }
                if (GRID[transform(i)][transform(j--)] == 1) {
                    countOfNeighbors++;
                }
            }
        }return countOfNeighbors;
    }

    int transform(int i) {                                                         // to do edges of an array one whole
        return ((i - 1) < 0) ? (GRID.length - 1) : (i > (GRID.length - 1)) ? 0 : i;
    }

/*
        public static cloneArray(tempGrid[][]) {          //
            int length = src.length;
            int[][] target = new int[length][src[0].length];
            for (int i = 0; i < length; i++) {
                System.arraycopy(src[i], 0, target[i], 0, src[i].length);
            }
            return target;
        }
*/
    public void nextGeneration() {

        for (int i = 0; i < GRID.length; i++) {
            for (int j = 0; j < GRID[i].length; j++) {

                int neighbors = numOfNeighbors();

                if (GRID[i][j] == 1) {
                    numOfNeighbors();
                    if (neighbors == 2 || neighbors == 3) {
                        tempGrid[i][j] = 1;
                    } else if (neighbors < 2 && neighbors > 3) {
                        tempGrid[i][j] = 0;
                    }
                    if (GRID[i][j] == 0) {
                        numOfNeighbors();
                        if (neighbors == 3) {
                            tempGrid[i][j] = 1;
                        }
                    }
                }
            }
        }
    }

    public void modifyGrid(){
        for (int i = 0; i < tempGrid.length; i++) {
            System.arraycopy(tempGrid[i], 0, GRID[i], 0, tempGrid[i].length);
        }
    }

    public void drawNext() {          // to print array on the screen
        for (int i = 0; i < GRID.length; i++) {
            for (int j = 0; j < GRID[i].length; j++) {
                if (GRID[i][j] == 0) {      //0 = " ", 1 = "Х"
                    System.out.print(" ");
                } else {
                    System.out.print("X");
                }
            }
            System.out.println();
        }
    }
}
